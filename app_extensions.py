from flask_sqlalchemy import SQLAlchemy
from flask_mongoengine import MongoEngine
from flask_marshmallow import Marshmallow
from flask_mail import Mail

db = MongoEngine()
ma = Marshmallow()
mail = Mail()
