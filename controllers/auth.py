from flask import Blueprint

auth_module = Blueprint('auth', __name__)


# ======================================================================================
# @desc      Demo Route
# @route     GET /api/v1/auth/verify_email/<token>
# @access    Public
# ======================================================================================
@auth_module.route('/demo', methods=['GET'])
def demo():
    return {'success': True, 'data': "Demo Route"}, 200

